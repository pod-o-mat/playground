# Playground

## Initial setup for domain name resolution

Edit the `/etc/hosts` file and add the list of service that you want to be resolve at the end of the line `127.0.0.1 localhost`. It should look like this:

```bash
127.0.0.1    localhost traefik.localhost portainer.localhost polynote.localhost minio-console.localhost minio.localhost dremio.localhost postgres.localhost dynamodb.localhost dynamodb-console.localhost
```

## Start the control-plane

```bash
./control-plane up -d
```

## Credentials

| Service   | Username  | Password | Comments                      |
|-----------|-----------|----------|-------------------------------|
| Portainer | portainer | password |                               |
| Minio     | minio     | password |                               |
| Grafana   | admin     | admin    |                               |
| Postgres  | postgres  | password | default database = "postgres" |


## Services

[Traefik    - Reverse proxy](http://traefik.localhost/)
[Portainer  - Docker manager](http://portainer.localhost/)
[Poynote    - Scala notebooks](http://polynote.localhost/)
[DynamoDB   - Console for dynamoDB](http://dynamodb-console.localhost/)
[Minio      - Console for minio buckets](http://minio-console.localhost/)

[Grafana    - Dashboards](http://grafana.localhost/)
[Prometheus - Metric collector](http://prometheus.localhost/)


## Create your playground

1. Open [Portainer](http://portainer.localhost/)
2. Login and click on the `local` docker icon
3. In the left bar click on the `> App Templates` to open the `Custom Tempaltes`
4. Click on `Custom Templates`
5. Click on the Stack you want to deploy (e.g. `Polynote`)
6. Click on the `Deploy the stack` button.
7. Repeat for all the stacks you want to play with.

## To stop the playground USE Portainer

1. Click on `Stacks` in the left bar
2. Select all the `Stacks`
3. Click the `Remove` button

Once they are all stopped

```bash
./control-plane down
```

## To remove all the DATA (clean everything)

All the buckets in Minio, all the tables in DynamoDB, all the data in Postgres and all other stack data will be ERASE forever!

```bash
./control-plane clean
````

## To connect localy 

### Postgres

```bash
psql -U postgres -h postgres.localhost -p 5432 postgres -c "SELECT datname,usename,application_name,client_addr,state,query from pg_stat_activity;"
```


## Trouble shoot

Traefik wants to use the local ports 
- 80 for HTTP and reverse proxy
- 5432 for postgres

If theses ports are already used, you can change them by editing the `docker-compose.yml` file (around line 8)

```yaml
    ports:
      - 80:80        # all HTTP
      - 5432:5432    # postgres
```

and change it to something like 

```yaml
    ports:
      - 8080:80        # all HTTP
      - 6543:5432    # postgres
```

To use port 8080 for HTTP requests and 6543 to connect to Postgres

[Traefik](http://traefik.localhost:8080/)
[Portainer ](http://portainer.localhost:8080/)
[Poynote](http://polynote.localhost:8080/)
[DynamoDB](http://dynamodb-console.localhost:8080/)
[Minio](http://minio-console.localhost:8080/)

[Grafana](http://grafana.localhost:8080/)
[Prometheus](http://prometheus.localhost:8080/)

Connect localy to postgres with port 6543 instead of 5432
```bash
psql -U postgres -h postgres.localhost -p 6543 postgres -c "SELECT datname,usename,application_name,client_addr,state,query from pg_stat_activity;"
```


